#!/bin/sh
# Debian xdm package post-installation script
# Copyright 1998-2001, 2003, 2004 Branden Robinson.
# Licensed under the GNU General Public License, version 2.  See the file
# /usr/share/common-licenses/GPL or <https://www.gnu.org/copyleft/gpl.txt>.
# Acknowledgements to Stephen Early, Mark Eichin, and Manoj Srivastava.

set -e

# source debconf library
. /usr/share/debconf/confmodule

THIS_PACKAGE=xdm
THIS_SCRIPT=postinst

#INCLUDE_SHELL_LIB#

DAEMON=/usr/bin/xdm
OLD_DAEMON=/usr/bin/X11/xdm

# debconf is not a registry, so we only fiddle with the default file if it
# does not exist
DEFAULT_DISPLAY_MANAGER_FILE=/etc/X11/default-display-manager
if ! [ -e "$DEFAULT_DISPLAY_MANAGER_FILE" ]; then
  DEFAULT_DISPLAY_MANAGER=
  if db_get shared/default-x-display-manager; then
    DEFAULT_DISPLAY_MANAGER="$RET"
  fi
  if [ -n "$DEFAULT_DISPLAY_MANAGER" ]; then
    DAEMON_NAME=
    if db_get "$DEFAULT_DISPLAY_MANAGER"/daemon_name; then
      DAEMON_NAME="$RET"
    fi
    if [ -z "$DAEMON_NAME" ]; then
      # if we were unable to determine the name of the selected daemon (for
      # instance, if the selected default display manager doesn't provide a
      # daemon_name question), guess
      DAEMON_NAME=$(which "$DEFAULT_DISPLAY_MANAGER" 2>/dev/null)
      if [ -z "$DAEMON_NAME" ]; then
        warn "unable to determine path to default X display manager" \
             "$DEFAULT_DISPLAY_MANAGER; not updating" \
             "$DEFAULT_DISPLAY_MANAGER_FILE"
      fi
    fi
    if [ -n "$DAEMON_NAME" ]; then
      if [ "$DAEMON_NAME" = "$OLD_DAEMON" ]; then
        DAEMON_NAME="$DAEMON"
      fi
      observe "committing change of default X display manager"
      echo "$DAEMON_NAME" > "$DEFAULT_DISPLAY_MANAGER_FILE"
    fi
  fi
else
  DEFAULT_DISPLAY_MANAGER=$(cat "$DEFAULT_DISPLAY_MANAGER_FILE")
  if [ "$DEFAULT_DISPLAY_MANAGER" = "$OLD_DAEMON" ]; then
    observe "changing default X display manager from $OLD_DAEMON to $DAEMON"
    echo "$DAEMON" > "$DEFAULT_DISPLAY_MANAGER_FILE"
  fi
fi

# remove the displaced old default display manager file if it exists
if [ -e "$DEFAULT_DISPLAY_MANAGER_FILE.dpkg-tmp" ]; then
  rm "$DEFAULT_DISPLAY_MANAGER_FILE.dpkg-tmp"
fi

DEFAULT_SERVICE=/etc/systemd/system/display-manager.service
# set default-display-manager systemd service link according to our config
if [ "$1" = configure ] && [ -d /etc/systemd/system/ ]; then
  if [ -e "$DEFAULT_DISPLAY_MANAGER_FILE" ]; then
    SERVICE=/lib/systemd/system/$(basename $(cat "$DEFAULT_DISPLAY_MANAGER_FILE")).service
    if [ -h "$DEFAULT_SERVICE" ] && [ $(readlink "$DEFAULT_SERVICE") = /dev/null ]; then
      echo "Display manager service is masked" >&2
    elif [ -e "$SERVICE" ]; then
      ln -sf "$SERVICE" "$DEFAULT_SERVICE"
    else
      echo "WARNING: $SERVICE is the selected default display manager but does not exist" >&2
      rm -f "$DEFAULT_SERVICE"
    fi
  else
    rm -f "$DEFAULT_SERVICE"
  fi
fi

# Registering the init scripts or starting the daemon may cause output to
# stdout, which can confuse debconf.
db_stop

if [ -e /etc/init.d/xdm ]; then
  update-rc.d xdm defaults 99 01
fi

# Whether we are installing or upgrading, we check the options file to see if
# the user wants the daemon (re-)started.
NOSTART=
XDM_WHERE=
if [ -e /var/run/xdm.install ]; then
  # Don't start the daemon if the options file says not to.
  if ! grep -qs ^start-on-install /etc/X11/xdm/xdm.options; then
    NOSTART=yes
  fi
else
  # We are upgrading.  Don't start the daemon if the options file says not to.
  if ! grep -qs ^restart-on-upgrade /etc/X11/xdm/xdm.options; then
    NOSTART=yes
  fi
fi

# At this point we may think we *should* be starting the daemon, but we need to
# do some more checks.  Clean up the old, obsolete /var/state/xdm directory
# (which we can only do if the daemon isn't running).

DENYSTART=
# On upgrades, don't start the daemon if it's already running...
if [ -z "$FIRSTINST" ] && \
  start-stop-daemon --stop --quiet --signal 0 --pid /var/run/xdm.pid \
                     --name $(basename $DAEMON) ; then
  # Note our refusal to start the daemon if we were supposed to start it.
  [ -n "$NOSTART" ] || DENYSTART=yes
  DENIAL_REASON="xdm is already running at pid $(cat /var/run/xdm.pid)"
  if [ -d /var/state/xdm ]; then
    warn "obsolete directory /var/state/xdm cannot be removed because" \
         "$DENIAL_REASON; reinstall the xdm package (or remove the directory" \
         "manually) when xdm is not running"
  fi
else
  if [ -d /var/state/xdm ]; then
    rm -r /var/state/xdm
  fi
  # ...or if we're currently in X on any of the displays it attempts to manage
  # by default.
  if [ -s /etc/X11/xdm/Xservers ]; then
    MANAGED_DISPLAYS="$(egrep -v '^[[:space:]]*#' /etc/X11/xdm/Xservers \
                        | sed 's/[[:space:]].*//')"
    if [ -n "$MANAGED_DISPLAYS" ]; then
      for MANAGED_DISPLAY in $MANAGED_DISPLAYS; do
        if echo "$DISPLAY" | grep -q "^$MANAGED_DISPLAY"; then
          # Note our refusal to start the daemon if we were supposed to start
          # it.
          [ -n "$NOSTART" ] || DENYSTART=yes
          DENIAL_REASON="an X server is already running at $DISPLAY, which"
          DENIAL_REASON="$DENIAL_REASON xdm is configured to to manage"
          break
        fi
      done
    fi
  fi
fi

# If the user wanted us to start the daemon but we refuse, explain why.
if [ -n "$DENYSTART" ]; then
  warn "not starting xdm because $DENIAL_REASON"
  NOSTART=yes
fi

[ -n "$NOSTART" ] || invoke-rc.d xdm start || true

#DEBHELPER#

# Remove install flag file.  Leave the "daemon not stopped" flag file, if it
# exists, so that it will be seen by the init script.
rm -f /var/run/xdm.install

exit 0

# vim:set ai et sts=2 sw=2 tw=80:
